# Scope (12p)

## Design (4p)
* 1 Fancy animations
* 1 No UI Framework
* 2 Responsive Design

## Functionality (6p)
* 4 Scraping
* 2 Infinity scroll

## Testing (2p)
* 2 Unit testing

# Prereq
* pnpm (https://pnpm.io/installation)
* node
* http-server (npm install -G http-server)

# Install dependencies
pnpm install

# Build
pnpm build

# Test
pnpm test

# Run backend (:port 3000)
node packages/backend/dist/index.js

# Run frontend (:port 8080)
http-server packages/frontend/dist/

