import { Ninja } from '@monorepo/commons'

const numberOfNinjas:number = 272;
const limit:number = 30;
const pages:number = numberOfNinjas/limit;

let page:number = 0;

function addScrollListener() {
    document.addEventListener('scroll',scrollHandler);
}

function scrollHandler() {
    let lastNinja:string = "last-ninja-" + (page -1);
    let scrollListener:HTMLElement | null = document.getElementById(lastNinja);
    if(scrollListener) {
        if ( (window.scrollY + window.outerHeight) > scrollListener.offsetTop && page < pages) {
            loadMoreNinjas(page++);
            document.removeEventListener('scroll',scrollHandler);
            addScrollListener();
        }
    }
}

function loadMoreNinjas(page: number) {
    let myDiv:HTMLElement | null = document.getElementById("ninjas-container");
    fetch('http://localhost:3000/api?limit=' + limit + '&page=' + page).then(result => result.text()).then(textformat => {
        let ninjas:Ninja[] = JSON.parse(textformat);

        let ninjasInPyama:string = "";
        for(let i = 0; ninjas.length > i; i++) {
            let ninjaInPyama:string = "";
            if(i == (ninjas.length -1)) {
                ninjaInPyama += "<div id=\"last-ninja-" + page +  "\" class=\"ninja\">";
            } else {
                ninjaInPyama += "<div class=\"ninja\">";
            }
            ninjaInPyama += "<img src=\"" + ninjas[i].img + "\"/>";
            ninjaInPyama += "<div class=\"name\">" + ninjas[i].name + "</div>";
            ninjaInPyama += "<div class=\"location\">" + ninjas[i].city + " "  + ninjas[i].country  + " </div>";
            ninjaInPyama += "</div>";
            ninjasInPyama += ninjaInPyama;
        }
        if(myDiv) {
            myDiv.innerHTML += ninjasInPyama;
        }
        let iteration = 0;

        let elementsByClassName:any;

        if(myDiv) {
            elementsByClassName = myDiv.getElementsByClassName("ninja");
        }

        if(elementsByClassName !== null) {
            for (const elementsByClassNameElement of elementsByClassName) {
                if(elementsByClassNameElement.className === "ninja") {
                    setTimeout(function(){
                        elementsByClassNameElement.setAttribute("class", "ninja load");
                    }, 100 * iteration++);
                }
            }
        }
    });

}

document.addEventListener("DOMContentLoaded", (event) => {
    loadMoreNinjas(page++);
});

addScrollListener();
