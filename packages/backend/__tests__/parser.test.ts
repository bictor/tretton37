import parseNinjas from "../src/ninjaParser";

describe('Dummy test', () => {
  it('Dummy test', () => {
    expect(parseNinjas).toBeDefined()
    const output = parseNinjas("<div class=\"ninja-summary\" data-aos=\"flip-up\" data-aos-offset=\"-20\"> <div class=\"contact-info\"> <h1><a href=\"meet/sofia-larsson\">Sofia Larsson<span>🇸🇪 Lund</span></a></h1> <nav class=\"ninja-nav\"> <a href=\"/meet/sofia-larsson\" class=\"btn-secondary btn-small\"><span>Get to know me</span></a> </nav> </div> <a href=\"/meet/sofia-larsson\" data-bind=\"click: isActiveFilter(filters.grid) ? selectNinja : null\"> <img class=\"portrait\" src=\"https://i.1337co.de/profile/sofia-larsson-medium\" alt=\"Sofia Larsson\"> </a> </div>")
    expect(output).toMatchObject([{"city": "🇸🇪", "country": "Lund", "img": "https://i.1337co.de/profile/sofia-larsson-medium", "name": "Sofia Larsson", "path": "meet/sofia-larsson"}])
  })
})
