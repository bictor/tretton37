import express, {Request, Response} from 'express';
import * as fs from 'fs';
import { Ninja } from '@monorepo/commons'
import ninjaPaser from "./ninjaParser";
import axios from "axios";

const app = express();

const url = "https://tretton37.com/meet";
let ninjas:Ninja[] = JSON.parse(fs.readFileSync('packages/backend/resources/ninjas.json','utf8'));

app.get('/api', (req:any, res:any) => {
    try {
        res.header('Access-Control-Allow-Origin', '*');
        // Pagination
        let limit: number = Number(req.query.limit) || ninjas.length - 1;
        let page:number = Number(req.query.page) || 0;
        console.log("Fetching Ninjas with limit: " + limit + " and page: " + page);
        return res.send(ninjas.slice(page * limit,(page + 1) * limit));
    } catch (e) {
        console.log(e);
    }
});

app.get('/api/count', (req:any, res:any) => {
    try {
        res.header('Access-Control-Allow-Origin', '*');
        console.log("Fetching Ninja count");
        return res.sendStatus(ninjas.length);
    } catch (e) {
        console.log(e);
    }
});

app.listen(3000);
console.log("Serving " + ninjas.length + " ninjas via API");

async function primeCache() {
    try {
        const response = await axios.get(url);
        // Allow for quick startup and backfeed with realtime data without interruption
        let parsedNinjas:Ninja[] = ninjaPaser(response.data);
        if(JSON.stringify(parsedNinjas) !== JSON.stringify(ninjas)) {
            ninjas = parsedNinjas;
            fs.writeFileSync("packages/backend/resources/ninjas.json", JSON.stringify(parsedNinjas),'utf8')
            console.log("Updated file, now housing " + ninjas.length + " ninjas.");
        }
    } catch (exception) {
        process.stderr.write(`ERROR received from ${url}: ${exception}\n`);
    }
}
primeCache();