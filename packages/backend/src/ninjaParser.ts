import BeautifulDom  from "beautiful-dom";
import HTMLElementData from "beautiful-dom/dist/htmlelement";
import { Ninja } from '@monorepo/commons'

const parseNinjas = (html: string) => {
    let ninjas:Ninja[] = new Array();
    let iterator = 0;
    const fullDom = new BeautifulDom(html);

    // Pyama is this case is HTML
    const ninjasInPyjamas:HTMLElementData[] = fullDom.getElementsByClassName("ninja-summary");
    for (const ninjaInPyjama of ninjasInPyjamas) {
        const ninja:Ninja = new Ninja();
        const beautifulNinja = new BeautifulDom(ninjaInPyjama.outerHTML);
        const image:HTMLElementData[] = beautifulNinja.getElementsByTagName("img");
        if(image.length > 0) {
            ninja.setImg(image[0].getAttribute("src"));
            ninja.setName(image[0].getAttribute("alt"))
        }

        const location:HTMLElementData | null = beautifulNinja.querySelector("span");
        if(location?.innerText.split(" ") != undefined && location?.innerText.split(" ").length > 0) {
            ninja.setCity(location?.innerText.split(" ")[0]);
            ninja.setCountry(location?.innerText.split(" ")[1]);
        }
        ninja.setPath(beautifulNinja.querySelector("a")?.getAttribute("href"))
        ninjas[iterator++] = ninja;
    }
    return ninjas;
};

export default parseNinjas;
