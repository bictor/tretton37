export class Ninja {

  public name: string | undefined;
  public country: string  | undefined;
  public city: string  | undefined;
  public path: string  | undefined;
  public img: string | undefined;

  constructor(name?: string, country?: string, city?: string, path?: string, img?: string) {
    this.name = name;
    this.country = country;
    this.city = city;
    this.path = path;
    this.img = img;
  }

  setImg(img: string | null) {
    if(img) {
      this.img=img;
    }
  }

  setName(name: string | null) {
    if(name) {
      this.name=name;
    }
  }

  setCountry(country: string | undefined) {
    if(country)
      this.country=country;
  }

  setCity(city: string | null | undefined) {
    if(city)
      this.city=city
  }

  setPath(path: string | null | undefined) {
    if(path)
      this.path=path
  }
}

export default { Ninja }